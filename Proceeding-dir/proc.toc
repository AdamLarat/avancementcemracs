\select@language {english}
\contentsline {section}{\tocsection {}{1}{Introduction}}{1}
\contentsline {section}{\tocsection {}{2}{p4est Library: Description and Specificities}}{2}
\contentsline {subsection}{\tocsubsection {}{2.1}{Presentation of AMR Techniques}}{2}
\contentsline {subsubsection}{\tocsubsubsection {}{}{Block-based AMR}}{2}
\contentsline {subsubsection}{\tocsubsubsection {}{}{Cell-based AMR}}{2}
\contentsline {subsection}{\tocsubsection {}{2.2}{How does \texttt {p4est}~deal with meshes?}}{3}
\contentsline {subsubsection}{\tocsubsubsection {}{2.2.1}{Encoding the Forest}}{4}
\contentsline {subsection}{\tocsubsection {}{2.3}{Creating a new mesh or adapting the current mesh}}{5}
\contentsline {section}{\tocsection {}{3}{Model and Numerical Methods}}{6}
\contentsline {subsection}{\tocsubsection {}{3.1}{A Two-fluid Model}}{6}
\contentsline {subsubsection}{\tocsubsubsection {}{}{Thermodynamics}}{6}
\contentsline {subsubsection}{\tocsubsubsection {}{}{Kinematics}}{7}
\contentsline {subsubsection}{\tocsubsubsection {}{}{Mathematical Properties}}{7}
\contentsline {subsection}{\tocsubsection {}{3.2}{Finite Volume Relaxation Method}}{7}
\contentsline {subsubsection}{\tocsubsubsection {}{}{Dimensional Splitting and Finite Volume Discretization}}{7}
\contentsline {subsubsection}{\tocsubsubsection {}{}{First order Suliciu's relaxation method}}{8}
\contentsline {subsubsection}{\tocsubsubsection {}{}{Time step and CFL condition}}{9}
\contentsline {subsection}{\tocsubsection {}{3.3}{Higher-order schemes}}{9}
\contentsline {subsubsection}{\tocsubsubsection {}{}{Second order in space: MUSCL method and slope limiters}}{9}
\contentsline {subsubsection}{\tocsubsubsection {}{}{Second order in time: the MUSCL-Hancock scheme}}{10}
\contentsline {subsubsection}{\tocsubsubsection {}{}{Strang splitting}}{10}
\contentsline {section}{\tocsection {}{4}{Results}}{11}
\contentsline {subsection}{\tocsubsection {}{4.1}{Performance tests}}{11}
\contentsline {subsubsection}{\tocsubsubsection {}{}{Accuracy, order of convergence and computational times}}{11}
\contentsline {subsubsection}{\tocsubsubsection {}{}{Parallel performance}}{12}
\contentsline {subsubsection}{\tocsubsubsection {}{}{Conclusion about performance}}{15}
\contentsline {subsection}{\tocsubsection {}{4.2}{Sod and double rarefaction test cases}}{15}
\contentsline {subsection}{\tocsubsection {}{4.3}{Other simulations}}{16}
\contentsline {paragraph}{\tocparagraph {}{}{ \bf 3D simulations ?}}{17}
\contentsline {section}{\tocsection {}{5}{Conclusion}}{17}
\contentsline {section}{\tocsection {}{}{References}}{18}
