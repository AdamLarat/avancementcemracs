\documentclass[xcolor=svgnames]{beamer}
\usepackage{texformat}

\usepackage{verbatim}
\usepackage{caption}
\usepackage[ruled,linesnumbered]{algorithm2e}
\usepackage{tikz}

\usepackage{pgfplots}
\usepackage{pgfplotstable}

\usepgfplotslibrary{external}
\tikzexternalize

% suppress navigation bar
\beamertemplatenavigationsymbolsempty

\mode<presentation>
{
  \usetheme{bunsen}
  \setbeamertemplate{items}[square]
}

\newenvironment{itemize*}%
  {\begin{itemize}%
    \setlength{\itemsep}{15pt}%
    \setlength{\parskip}{0pt}}%
  {\end{itemize}}

% set fonts
\setbeamerfont{frametitle}{size=\LARGE,series=\bfseries}

\definecolor{uipoppy}{RGB}{225, 64, 5}
\definecolor{uipaleblue}{RGB}{96,123,139}
\definecolor{uiblack}{RGB}{0, 0, 0}

% caption styling
\DeclareCaptionFont{uiblack}{\color{uiblack}}
\DeclareCaptionFont{uipoppy}{\color{uipoppy}}
\captionsetup{labelfont={uipoppy},textfont=uiblack}

\renewcommand{\emph}[1]{\textcolor{MidnightBlue}{\textbf{#1}}}
\newcommand{\floor}[1]{\left\lfloor #1 \right\rfloor}
\newcommand{\ceil}[1]{\left\lceil #1 \right\rceil}
\newcommand*{\rfrac}[2]{{}^{#1}\!/_{#2}}

% add a vertical line between multicol columns
\setlength{\columnseprule}{0.4pt}

% title slide definition
\title{Adaptive Mesh Refinement with p4est}
\author{Alexandru Fikl}
\date{}

%--------------------------------------------------------------------
%                           Introduction
%--------------------------------------------------------------------

\begin{document}

\section{Introduction}
\setbeamertemplate{background}
{\includegraphics[width=\paperwidth,height=\paperheight]{img/frontpage_bg}}
\setbeamertemplate{footline}[default]

\begin{frame}
\vspace{2cm}
\begin{columns}
\begin{column}{0.4\textwidth}
  \titlepage
  \vspace{10cm}
\end{column}
\begin{column}{0.6\textwidth}
\end{column}
\end{columns}
\end{frame}

\setbeamertemplate{background}
 {\includegraphics[width=\paperwidth, height=\paperheight]{img/slide_bg}}
\setbeamertemplate{footline}[bunsentheme]

\section{AMR}
\begin{frame}{Some History}
\vspace{20px}
\begin{itemize}
    \item AMR was introduced in the 80s by a series of papers from
    Joseph Oliger, Martha Berger (his PhD student) and Philip Colella.
    \item This was the \emph{block-based} type of AMR.
    \item Philip Colella is also the creator of \emph{CHOMBO}, another big AMR
    library.
    \item \emph{Cell-based} AMR was heavily inspired by the use of quad and
    octrees in computer graphics.
    \item In the early 90s it started getting used in aeronautics (work by
    \emph{David P. Young, Robin G. Melvin et al.}).
    \item Further improvements with the work of Alexei M. Khokhlov and his
    \emph{Fully Threaded Tree} in 1997.
\end{itemize}
\end{frame}

\begin{frame}{Block-based}
\begin{columns}
\column{0.5\textwidth}
\begin{center}
\begin{tikzpicture}[scale=0.5]

\begin{scope}[yslant=0.5, xslant=-1]
\draw [black, fill=blue!20, thick] (0, 0) grid (5, 5) rectangle (0, 0);
\end{scope}

\begin{scope}[yshift=2.2cm, yslant=0.5, xslant=-1]
\draw [black, thick, fill=red!20, step=0.5] (0, 1.99) grid (5, 5) rectangle (0, 2);
\end{scope}

\begin{scope}[yshift=4.2cm, yslant=0.5, xslant=-1]
\draw [black, thick, fill=green!20, step=0.25] (2.99, 2.99) grid (5, 5) rectangle (3, 3);
\end{scope}

\draw [thick, red] (-5, 4.7) -- (-5, 2.5);
\draw [thick, red] (-2, 3.2) -- (-2, 1);
\draw [thick, red] (3, 5.7) -- (3, 3.5);

\draw [thick, red] (-2, 8.2) -- (-2, 6.2);
\draw [thick, red] (0, 7.2) -- (0, 5.2);
\draw [thick, red] (2, 8.2) -- (2, 6.2);
\end{tikzpicture}
\end{center}
\column{0.5\textwidth}
\begin{itemize}
    \item The basic idea is to overlap grids of different sizes.
    \item Easy to use existing single-grid libraries.
    \item Unwieldy for turbulent flows.
\end{itemize}
\end{columns}

\vfill

Introduced in \emph{Adaptive Mesh Refinement for Hyperbolic Partial
Differential Equations} by J. Oliger and M. J. Berger (JCP, 1984).
\end{frame}

\begin{frame}{Cell-based}
\begin{columns}
\column{0.5\textwidth}
\begin{center}
\begin{tikzpicture}[scale=0.7]
\draw [thick] (0, 9) rectangle (0.5, 9.5);

\draw [thick] (-3, 7.5) rectangle (-2.5, 8);
\draw [thick, fill=green!40] (-1, 7.5) rectangle (-0.5, 8);
\draw [thick, fill=blue!40] (1, 7.5) rectangle (1.5, 8);
\draw [thick] (3, 7.5) rectangle (3.5, 8);

\newcommand{\xs}{-3}
\foreach \x in {\xs, \xs + 2, \xs + 4, \xs + 6} {
    \draw [thick] (\x + 0.25, 8) -- (0.25, 9);
}

\renewcommand{\xs}{-4.2}
\foreach \x in {\xs, \xs + 0.8, \xs + 1.6, \xs + 2.4} {
    \draw [thick] (\x + 0.25, 6) -- (-2.75, 7.5);
    \draw [thick, fill=red!50] (\x, 5.5) rectangle (\x + 0.5, 6);
}

\renewcommand{\xs}{1.8}
\foreach \x in {\xs, \xs + 0.8, \xs + 1.6, \xs + 2.4} {
    \draw [thick] (\x + 0.25, 6) -- (3.25, 7.5);
    \draw [thick, fill=orange!50] (\x, 5.5) rectangle (\x + 0.5, 6);
}
\end{tikzpicture}
\end{center}

\column{0.5\textwidth}
\begin{center}
\begin{tikzpicture}[scale=1.5]
\draw [thick] (0, 0) grid (2, 2);
\draw [thick,fill=red!40,step=0.5] (0, 0) grid (1, 1) rectangle (0, 0);
\draw [thick,fill=orange!40,step=0.5] (1, 1) grid (2, 2) rectangle (1, 1);
\draw [fill=blue!40] (0, 1) rectangle (1, 2);
\draw [fill=green!40] (1, 0) rectangle (2, 1);
\end{tikzpicture}
\end{center}
\end{columns}

\vfill

\small
\begin{itemize}
    \item Cell-based AMR relies on a \emph{tree structure} (quadtree or
    octree) to partition the space.
    \item The \emph{leaves} are the only ones that represent a part of the domain.
    \item Tree based algorithms are generally more \emph{complicated} than those used
    on Cartesian grids.
    \item Storing the whole tree is usually a problem.
\end{itemize}
\end{frame}

\begin{frame}{Comparison}
\begin{figure}[h!]
    \centering
    \includegraphics[width=0.7\linewidth]{./img/comparison.jpg}
    % comparison.jpg: 0x0 pixel, 300dpi, 0.00x0.00 cm, bb=
    \caption{Block-based (right) and Cell-based (left) Refinement.}
\end{figure}

From \emph{Efficiency Gains from Time Refinement on AMR Meshes and Explicit
Timestepping}, L. J. Dursi 1 and M. Zingale.
\end{frame}

\begin{frame}{Unstructured AMR}
\vfill
\begin{figure}[h!]
    \centering
    \includegraphics[width=0.5\linewidth]{./img/mandelbrot.jpg}
\end{figure}
\vfill
From \url{http://mfquant.net/gallery_fun.html}.
\end{frame}

\section{p4est}
\begin{frame}{Introduction}
\begin{itemize}
    \item Cell-based using a \emph{linear tree representation}.
    \item \emph{Cube}-like geometry $[0, 1]^d$ that can be transformed using a mapping:
    \[
    \phi_k : [0, 1]^d \to \mathbb{R}^d
    \]
    \item Each cube is a quad / octree.
    \item Complex geometries are achieved by \emph{composing multiple trees}
    (into a forest).
    \item The forest is almost completely distributed between the processes.
    (some connectivity information is still duplicated)
    \item
\end{itemize}
\end{frame}

\begin{frame}{Morton Ordering (z-index)}
\begin{columns}
\column{0.5\textwidth}
\begin{center}
\begin{tikzpicture}[scale=0.7]
\draw [thick] (0, 9) rectangle (0.5, 9.5);

\draw [thick] (-3, 7.5) rectangle (-2.5, 8);
\draw [thick, fill=green!40] (-1, 7.5) rectangle (-0.5, 8);
\draw [thick, fill=blue!40] (1, 7.5) rectangle (1.5, 8);
\draw [thick] (3, 7.5) rectangle (3.5, 8);

\newcommand{\xs}{-3}
\foreach \x in {\xs, \xs + 2, \xs + 4, \xs + 6} {
    \draw [thick] (\x + 0.25, 8) -- (0.25, 9);
}

\renewcommand{\xs}{-4.2}
\foreach \x in {\xs, \xs + 0.8, \xs + 1.6, \xs + 2.4} {
    \draw [thick] (\x + 0.25, 6) -- (-2.75, 7.5);
    \draw [thick, fill=red!50] (\x, 5.5) rectangle (\x + 0.5, 6);
}

\renewcommand{\xs}{1.8}
\foreach \x in {\xs, \xs + 0.8, \xs + 1.6, \xs + 2.4} {
    \draw [thick] (\x + 0.25, 6) -- (3.25, 7.5);
    \draw [thick, fill=orange!50] (\x, 5.5) rectangle (\x + 0.5, 6);
}

\draw[->] [thick, dashed] (-3.9, 5.75) -- (-1.6, 5.75);
\draw[->] [thick, dashed] (-1.6, 5.75) -- (-0.75, 7.75) -- (1.25, 7.75);
\draw[->] [thick, dashed] (1.25, 7.75) -- (2.05, 5.75) -- (4.5, 5.75);

\node at (0.25, 10) {Root};
\node at (-3.95, 5) {$0$};
\node at (-3.15, 5) {$1$};
\node at (-2.35, 5) {$2$};
\node at (-1.55, 5) {$3$};

\node at (-0.8, 7) {$4$};
\node at (1.25, 7) {$5$};

\node at (2.05, 5) {$6$};
\node at (2.85, 5) {$7$};
\node at (3.65, 5) {$8$};
\node at (4.45, 5) {$9$};
\end{tikzpicture}
\end{center}

\column{0.5\textwidth}
\begin{center}
\begin{tikzpicture}[scale=1.5]
\draw [thick] (0, 0) grid (2, 2);
\draw [thick,fill=red!40,step=0.5] (0, 0) grid (1, 1) rectangle (0, 0);
\draw [thick,fill=orange!40,step=0.5] (1, 1) grid (2, 2) rectangle (1, 1);
\draw [fill=blue!40] (0, 1) rectangle (1, 2);
\draw [fill=green!40] (1, 0) rectangle (2, 1);

\draw [thick, dashed] (0.25, 0.25) -- (0.75, 0.25);
\draw [thick, dashed] (0.75, 0.25) -- (0.25, 0.75);
\draw [thick, dashed] (0.25, 0.75) -- (0.75, 0.75);
\draw [thick, dashed] (0.75, 0.75) -- (1.5, 0.5);
\draw [thick, dashed] (1.5, 0.5) -- (0.5, 1.5);
\draw [thick, dashed] (0.5, 1.5) -- (1.25, 1.25);
\draw [thick, dashed] (1.25, 1.25) -- (1.75, 1.25);
\draw [thick, dashed] (1.75, 1.25) -- (1.25, 1.75);
\draw [thick, dashed] (1.25, 1.75) -- (1.75, 1.75);

\node at (0.25, 0.25) {$0$};
\node at (0.75, 0.25) {$1$};
\node at (0.25, 0.75) {$2$};
\node at (0.75, 0.75) {$3$};
\node at (1.5, 0.5) {$4$};
\node at (0.5, 1.5) {$5$};
\node at (1.25, 1.25) {$6$};
\node at (1.75, 1.25) {$7$};
\node at (1.25, 1.75) {$8$};
\node at (1.75, 1.75) {$9$};
\end{tikzpicture}
\end{center}
\end{columns}

Given the tree, the Morton ordering is just a \emph{preorder} traversal.
An important characteristic of space filling curves such as this is
\emph{cache locality}.

\end{frame}

\begin{frame}[fragile]{Morton Ordering (z-index) II}
\begin{columns}
\column{0.4\textwidth}
\begin{itemize}
    \item Morton index for quadrant $6$ (plus level):
    \[
    11~ 00~ (10).
    \]
    \item To get $(x, y)$:
    \begin{verbatim}
    11 00
    _1 _0 = 2 = x
    1_ 0_ = 2 = y
    \end{verbatim}
\end{itemize}

The $(x, y)$ we got represent the coordinates of the cell if the mesh
was uniform on level $l$ (in this case 2).

\column{0.6\textwidth}
\begin{center}
\begin{tikzpicture}[scale=0.7]
\draw [thick] (0, 9) rectangle (0.5, 9.5);

\draw [thick] (-3, 7.5) rectangle (-2.5, 8);
\draw [thick, fill=green!40] (-1, 7.5) rectangle (-0.5, 8);
\draw [thick, fill=blue!40] (1, 7.5) rectangle (1.5, 8);
\draw [thick] (3, 7.5) rectangle (3.5, 8);

\newcommand{\xs}{-3}
\foreach \x in {\xs, \xs + 2, \xs + 4, \xs + 6} {
    \draw [thick] (\x + 0.25, 8) -- (0.25, 9);
}

\renewcommand{\xs}{-4.2}
\foreach \x in {\xs, \xs + 0.8, \xs + 1.6, \xs + 2.4} {
    \draw [thick] (\x + 0.25, 6) -- (-2.75, 7.5);
    \draw [thick, fill=red!50] (\x, 5.5) rectangle (\x + 0.5, 6);
}

\renewcommand{\xs}{1.8}
\foreach \x in {\xs, \xs + 0.8, \xs + 1.6, \xs + 2.4} {
    \draw [thick] (\x + 0.25, 6) -- (3.25, 7.5);
    \draw [thick, fill=orange!50] (\x, 5.5) rectangle (\x + 0.5, 6);
}

\node at (-3.95, 5) {$0$};
\node at (-3.15, 5) {$1$};
\node at (-2.35, 5) {$2$};
\node at (-1.55, 5) {$3$};

\node at (-0.8, 7) {$4$};
\node at (1.25, 7) {$5$};

\node at (2.05, 5) {$6$};
\node at (2.85, 5) {$7$};
\node at (3.65, 5) {$8$};
\node at (4.45, 5) {$9$};

\node at (-2, 7.73) {$00$};
\node at (0, 7.73) {$01$};
\node at (2, 7.73) {$10$};
\node [thick, red] at (4, 7.73) {$11$};
\node [thick, red] at (2.05, 4.3) {$00$};
\node at (2.85, 4.3) {$01$};
\node at (3.65, 4.3) {$10$};
\node at (4.45, 4.3) {$11$};
\end{tikzpicture}
\end{center}
\end{columns}
\end{frame}

\begin{frame}{Connectivity}
\begin{center}
\begin{tikzpicture}
\draw [fill=blue!40] (0, 0) rectangle (5, 5);
\draw[->] [thick] (0, 0) -- (6, 0);
\draw[->] [thick] (0, 0) -- (0, 6);

\node at (5.8, -0.25) {$x$};
\node at (-0.25, 5.8) {$y$};

\node at (-.2, -0.2) {$c_0$};
\node at (5.2, -0.2) {$c_1$};
\node at (5.2, 5.2) {$c_2$};
\node at (-0.2, 5.2) {$c_3$};
\node at (-0.25, 2.5) {$f_0$};
\node at (5.25, 2.5) {$f_1$};
\node at (2.5, -0.25) {$f_2$};
\node at (2.5, 5.25) {$f_3$};
\end{tikzpicture}
\end{center}
\end{frame}

\begin{frame}[fragile]{Traversing the Tree}
\vspace{10px}
\begin{algorithm}[H]
  \caption{Get Child.}
  \KwData{octant $o$, child id $i$}
  $h \leftarrow 2^{b - (o.l + 1)}$\;
  $c.l = o.l + 1$\;
  $c.x \leftarrow o.x ~|~ (i \& 1 ~?~ h : 0)$\;
  $c.y \leftarrow o.y ~|~ (i \& 2 ~?~ h : 0)$\;
  $c.z \leftarrow o.z ~|~ (i \& 4 ~?~ h : 0)$\;
\end{algorithm}

\begin{algorithm}[H]
  \caption{Get Parent.}
  \KwData{octant $o$}
  $h \leftarrow 2^{b - o.l}$\;
  $q.l \leftarrow o.l - 1$\;
  $q.x \leftarrow o.x \& \neg h$\;
  $q.y \leftarrow o.y \& \neg h$\;
  $q.z \leftarrow o.z \& \neg h$\;
\end{algorithm}
\end{frame}

\begin{frame}{Finding Face Neighbors}
\begin{algorithm}[H]
  \caption{Get Face Neighbor.}
  \KwData{octant $o$, face $f$}
  $h \leftarrow 2^{b - o.l}$\;
  $q.l \leftarrow o.l$\;
  $q.x \leftarrow o.x + ((face = 0) ~?~ -h : (face = 1) ~?~ h : 0)$\;
  $q.y \leftarrow o.y + ((face = 2) ~?~ -h : (face = 3) ~?~ h : 0)$\;
  $q.z \leftarrow o.z + ((face = 4) ~?~ -h : (face = 5) ~?~ h : 0)$\;
\end{algorithm}
\end{frame}

\begin{frame}{Finding leaves}
\begin{itemize}
    \item Previous transformations are not very useful when using p4est.
    \item They create quadrants on the fly and do not give the ones in the
    containing the user data.
    \item The Morton index does not provide a way to find leaves in $O(1)$
    either.
    \item Efficient search algorithms are described in: \emph{Recursive
    Algorithms for Distributed Forests of Octrees}, Tobin Isaac,
    Carsten Burstedde, Lucas C. Wilcox, Omar Ghattas, 2014.
    \item You should use the mesh \texttt{p4est\_mesh\_t} if you need extensive
    connectivity information and $\mathcal{O}(1)$ algorithms to get the user
    data of a quadrant and its neighbors.
\end{itemize}
\end{frame}

\section{Algorithms}
\begin{frame}{Constructing a Uniform Mesh}
\footnotesize
\vspace{10px}
\begin{algorithm}[H]
  \caption{New Forest.}
  \KwData{Minimum quadrants per process $n_q$, number of trees $K$}
  $l = \ceil{\log_2 \ceil{\max{P n_q / K, 1} / d}}$\tcc*[r]{mimimum level}
  $n = 2^{dl}$\tcc*[r]{quadrants per tree}
  $N = n K$\tcc*[r]{global number of quadrants}
  $g_{first} = \floor{N p / P}$\tcc*[r]{first quadrant in process}
  $g_{last} = \floor{N (p + 1) / P}$\tcc*[r]{last quadrant in process}
  $k_{first} = \floor{\rfrac{g_{first}}{n}}$\tcc*[r]{first tree in process}
  $k_{last} = \floor{\rfrac{g_{last}}{n}}$\tcc*[r]{last tree in process}
  \For{$k \in \{k_{first}, \dots, k_{last}\}$}{
    $m_{first} = (k = k_{first} ~?~ (g_{first} - nk) : 0)$\;
    $m_{last} = (k = k_{last} ~?~ (g_{last} - nk) : n - 1)$\;
    $\mathcal{O}_k = \emptyset$\;
    \For{$m \in \{m_{first}, \dots, m_{last}\}$}{
        \tcc{\texttt{Octant} computes $(x, y, z)$ from the linear index}
        $\mathcal{O}_k \leftarrow \mathcal{O}_k \cup \mathrm{Octant}(l, m)$\;
    }
  }
\end{algorithm}
\end{frame}

\begin{frame}{Refining}
\begin{center}
\begin{tikzpicture}
\foreach \x in {1,..., 10} {
    \draw [fill=black!20] (\x, 0) rectangle (\x + 1, 1);
}

\draw [fill=green!20] (4, -3) rectangle (5, -2);
\draw [fill=green!20] (5.5, -3) rectangle (6.5, -2);
\draw [fill=green!20] (7, -3) rectangle (8, -2);

\draw [thick, red] (5.9, -0.1) -- (5.9, 1.2);
\draw [thick, red] (6.1, -0.1) -- (6.1, 1.2);
\draw[->] [thick, red] (5.7, -0.2) -- (6.3, -0.2);
\draw[->] [thick] (7, -2.5) -- (6.5, -2.5);
\draw[->] [thick] (5.5, -2.5) -- (5, -2.5);
\path[->] [thick] (6.5, -0.1) edge [out=-90, in=40] (8, -2.5);
\path[->] [thick] (4, -2.5) edge [out=160, in=-90] (5.5, -0.1);

\node at (6.5, 0.5) {$q$};
\node at (4.5, -2.5) {$o$};
\end{tikzpicture}
\end{center}
\begin{itemize}
    \item Quadrant array is \emph{grown} each time a quadrant is refined.
    \item A separate \emph{linked list} is kept to store the quadrants that
    have to be inserted.
\end{itemize}
\end{frame}

\begin{frame}{Partitioning}
\begin{algorithm}[H]
  \caption{Partition Forest for Equal Weights.}
  \KwData{$N$ global number of quadrants}
  \KwData{$P $ number of processes}
  \For{$p \leftarrow 0$ \KwTo $P$}{
    $N_p \leftarrow \floor{\frac{N(p + 1)}{P}} - \floor{\frac{N p}{P}}$\;
  }
  Partition\_given($N_p$) \tcc*[r]{exchange quadrants}
\end{algorithm}

\begin{itemize}
    \item \texttt{Partition\_given} will only exchange quadrants with
    neighboring processes because it relies on the \emph{z-curve};
    \item Due to this, if we have $(k_1, o_1) \leq (k_2, o_2)$, then
    the first pair is assigned to a $\leq$ process.
\end{itemize}
\end{frame}

\begin{frame}{Other Algorithms}
\vfill
Balancing, constructing the ghost layer and iterating are.. \emph{hard. :(}
\vfill
\end{frame}
\end{document}
